# README #

Release repo related to the dev repo ProjectView - EEH

### What is this repository for? ###

Patch releases to EEH PV staging and prod:

Minor updates to EEH PV often affect the front end only, and so a typical minor release would be these files:

* index.html     (the page in this single page application)
* bundle.js      (the bundled javascript front end)

If the release involves data updates there may also be SQL script(s) included in the release.
This repo would not be used if the server-side (django) code has changed.
Geoserver config is not version controlled in EEH PV. Changes to Geoserver layers and styles are performed from the admin console of the application.

### Deployment instructions ###

### Who do I talk to? ###

* Repo admin: Ian Sheppard